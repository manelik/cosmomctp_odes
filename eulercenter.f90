

program eulerCenter

! Solves a simple ODE as an IVP using euler method

! For this case we solve y'= -k y with k a positive constant

  implicit none

  real(8) :: u, u_p,u_pp, u_aux ! The numerical approximation and related functions
  
  real(8) :: v0

  real(8) :: x, dx  ! Grid parameters
  integer :: Nx

  integer :: i, j, k !counters

  character(30) :: out_dir !output directory


! Say Hello and set parameters
  write(*,*) "***********************************"
  write(*,*) "*                                 *"
  write(*,*) "*  Método de Euler Centrado       *"
  write(*,*) "*                                 *"
  write(*,*) "***********************************"
  write(*,*) "Posicion inicial x_0"
  read(*,*) x
  write(*,*) "Valor inicial u_0"
  read(*,*) u

  write(*,*) "Intervalo dx"
  read(*,*) dx
  write(*,*) "Puntos Nx"
  read(*,*) Nx

  write(*,*) "directorio de salida"
  read(*,*) out_dir

  
! Make directory
  call system('mkdir -p '//trim(out_dir))

! Open outfiles
  open(unit=88,file=trim(out_dir)//"/u.dat",status="unknown")

! save initial values
  write(88,"(2ES16.8)") x, u

! begin euler method

  do i = 1, Nx
! Save previous step value
     u_p = u
     u_pp= u_p
! Update using Euler's method
     u = u_pp + 2.D0*dx*DuDx(u_p,x)

! Advance x
     x = x+dx

! save to file
     write(88,"(2ES16.8)") x, u
     


  end do

  close(88)

  write(*,*) "Finished! Have a nice day."
! END
  contains

! The functions called in the main program

    function DuDx(u,x)
      implicit none
      
      real(8) DuDx
      real(8) u, x 

      DuDx = -u
    end function DuDx

  end program eulerCenter
