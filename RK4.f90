

program RK4_ODE

! Solves a simple ODE as an IVP using fourth order Runge-Kutta

! For this case we solve y'= -k y with k a positive constant

  implicit none

  real(8) :: u, u_p, u_aux ! The numerical approximation and related arrays
  real(8) :: u_k1,u_k2,u_k3,u_k4 ! coefficients of the runge kutta method
  real(8) :: x, dx  !grid parameters
  integer :: Nx

  integer :: i, j, k !counters

  character(30) :: out_dir ! output directory

  write(*,*) "******************"
  write(*,*) "*                *"
  write(*,*) "*  Metodo RK4    *"
  write(*,*) "*                *"
  write(*,*) "******************"
  write(*,*) "Posicion inicial x_0"
  read(*,*) x
  write(*,*) "Valor inicial u_0"
  read(*,*) u

  write(*,*) "Intervalo dx"
  read(*,*) dx
  write(*,*) "Puntos Nx"
  read(*,*) Nx

  write(*,*) "directorio de salida"
  read(*,*) out_dir

  
! Make directory
  call system('mkdir -p '//trim(out_dir))

! Open outfiles
  open(unit=88,file=trim(out_dir)//"/u.dat",status="unknown")

! save initial values
  write(88,"(2ES16.8)") x, u

! begin euler method

  do i = 1, Nx
! Save old time step
     u_p = u

! Evaluate RK4 coefficients     

     u_k1 = DuDx(u_p,x)

     u_k2 = DuDx( u_p + 0.5*dx*u_k1 , x+0.5*dx)
     u_k3 = DuDx( u_p + 0.5*dx*u_k2 , x+0.5*dx)

     u_k4 = DuDx( u_p + dx*u_k3 , x + dx)

! Update value     
     u = u_p + dx/6.0*(u_k1+2.0*u_k2+2.0*u_k3+u_k4)


! Advance x
     x = x+dx

! save to file
     write(88,"(2ES16.8)") x, u
     


  end do

  close(88)

  write(*,*) "Finished! Have a nice day."

  contains

    function DuDx(u,x)
      implicit none
      
      real(8) DuDx
      real(8) u, x 

      DuDx = -u
    end function DuDx

end program RK4_ODE
