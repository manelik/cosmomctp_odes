

program oscillator

! Solves a simple ODE as an IVP using fourth order Runge-Kutta

! For this case we solve y'= -k y with k a positive constant

  implicit none

  real(8) :: u, u_p, u_aux ! Amplitude
  real(8) :: u_k1,u_k2,u_k3,u_k4 ! coefficients of the runge kutta method
  real(8) :: v, v_p, v_aux ! Derivative
  real(8) :: v_k1,v_k2,v_k3,v_k4 ! coefficients of the runge kutta method
  real(8) :: t, dt  !grid parameters
  integer :: Nt

  integer :: i, j, k !counters

  character(30) :: out_dir ! output directory

  write(*,*) "******************************"
  write(*,*) "*                            *"
  write(*,*) "*  Oscilador armonico RK4    *"
  write(*,*) "*                            *"
  write(*,*) "******************************"

  write(*,*) "tiempo inicial"
  read(*,*) t
  write(*,*) "Valor inicial u_0"
  read(*,*) u
  write(*,*) "Valor inicial (derivada) v_0"
  read(*,*) v


  write(*,*) "Intervalo dt"
  read(*,*) dt
  write(*,*) "Pasos N"
  read(*,*) Nt

  write(*,*) "directorio de salida"
  read(*,*) out_dir

  
! Make directory
  call system('mkdir -p '//trim(out_dir))

! Open outfiles
  open(unit=88,file=trim(out_dir)//"/u.dat",status="unknown")
  open(unit=89,file=trim(out_dir)//"/v.dat",status="unknown")

! save initial values
  write(88,"(2ES16.8)") t, u
  write(89,"(2ES16.8)") t, v

! begin euler method

  do i = 1, Nt
! Save old time step
     u_p = u
     v_p = v

! Evaluate RK4 coefficients     

     u_k1 = DuDt(u_p,v_p,t)
     v_k1 = DvDt(u_p,v_p,t)

     u_k2 = DuDt( u_p + 0.5*dt*u_k1 ,v_p + 0.5*dt*v_k1 , t+0.5*dt)
     v_k2 = DvDt( u_p + 0.5*dt*u_k1 ,v_p + 0.5*dt*v_k1 , t+0.5*dt)

     u_k3 = DuDt( u_p + 0.5*dt*u_k2 ,v_p + 0.5*dt*v_k2 , t+0.5*dt)
     v_k3 = DvDt( u_p + 0.5*dt*u_k2 ,v_p + 0.5*dt*v_k2 , t+0.5*dt)

     u_k4 = DuDt( u_p + dt*u_k3 , v_p + dt*v_k3 , t + dt)
     v_k4 = DvDt( u_p + dt*u_k3 , v_p + dt*v_k3 , t + dt)

! Update values

     u = u_p + dt/6.0*(u_k1+2.0*u_k2+2.0*u_k3+u_k4)

     v = v_p + dt/6.0*(v_k1+2.0*v_k2+2.0*v_k3+v_k4)


! Advance time
     t = t+dt

! save to file
     write(88,"(2ES16.8)") t, u
     write(89,"(2ES16.8)") t, v
     

  end do

  close(88)
  close(89)

  write(*,*) "Finished! Have a nice day."

  contains

    function DuDt(u,v,t)
      implicit none
      
      real(8) DuDt
      real(8) u, v, t

      DuDt = v
    end function DuDt

    function DvDt(u,v,t)
      implicit none
      
      real(8) DvDt
      real(8) u, v, t 

      DvDt = -u !-0.1*v! +0.1*cos(0.5*t)
    end function DvDt


end program OSCILLATOR
