
#Makefile

#Compilador de Fortran
FC=gfortran

euler:	euler.f90
	gfortran euler.f90 -o euler

eulerbackwards:	eulerbackwards.f90
		gfortran eulerbackwards.f90 -o eulerbackwards

RK2:	RK2.f90
	gfortran RK2.f90 -o RK2

RK4:	RK4.f90
	gfortran RK4.f90 -o RK4
