

program eulerbackODE

! Solves a simple ODE as an IVP using implicit euler method

! For this case we solve y'= -k y with k a positive constant

  implicit none

  real(8) :: u, u_p, u_aux ! The numerical approximation and related arrays
  
  real(8) :: x, dx  ! grid parameters
  integer :: Nx

  integer :: i, j, k !counters

  character(30) :: out_dir

  write(*,*) "**************************"
  write(*,*) "*                        *"
  write(*,*) "*  Método de Euler       *"
  write(*,*) "*                        *"
  write(*,*) "**************************"
  write(*,*) "Posicion inicial x_0"
  read(*,*) x
  write(*,*) "Valor inicial u_0"
  read(*,*) u

  write(*,*) "Intervalo dx"
  read(*,*) dx
  write(*,*) "Puntos Nx"
  read(*,*) Nx

  write(*,*) "directorio de salida"
  read(*,*) out_dir

  
! Make directory
  call system('mkdir -p '//trim(out_dir))

! Open outfiles
  open(unit=88,file=trim(out_dir)//"/u.dat",status="unknown")

! save initial values
  write(88,"(2ES16.8)") x, u

! begin euler method

  do i = 1, Nx

! Save previous step
     u_p = u
! Update using implicit method, we assume that Su already inverts the 
! implicit equation    
     u = Su(u_p,x,dx)

! Advance x
     x = x+dx

! save to file
     write(88,"(2ES16.8)") x, u
     


  end do

  close(88)

  write(*,*) "Finished! Have a nice day."

  contains

    function Su(u,x,dx)
      ! the source of the evolution equation, once inverted
      implicit none
      
      real(8) Su
      real(8) u, x, dx

      Su = u/(1+dx)
    end function Su

end program eulerbackODE
